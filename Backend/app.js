require('dotenv').config();
require('./Auth/passport');
const express = require('express');
const passport = require('passport');
const userRoutes = require('./Routes/UserRoutes');
const eventRoutes = require('./Routes/EventRoutes');
const clubRoutes = require('./Routes/ClubRoutes');
const announcementRoutes = require('./Routes/AnnouncementRoutes');
const ticketRoutes = require('./Routes/TicketRoutes');
const authRoute = require('./Routes/AuthRoutes');
const config = require('./Config/config.js');
const database = require('./Database/Database.js');
const { EmailService } = require('./Services/EmailService');
const { FakeEmailService } = require('./Services/FakeEmailService');

const app = express();

app.use(passport.initialize());
app.use(passport.session());

app.use(express.urlencoded({
  extended: true,
}));
app.use(express.json());

app.use('/api/users', userRoutes);
app.use('/api/events', eventRoutes);
app.use('/api/clubs', clubRoutes);
app.use('/api/announcements', announcementRoutes);
app.use('/api/tickets', ticketRoutes);
app.use('/auth', authRoute);

database.connect();

global.emailService = config.email ? new EmailService() : new FakeEmailService();

const server = app.listen(config.port, () => {
  console.log(`Now listening on port ${config.port}`);
});

function stop() {
  console.log('stopping');
  server.close();
  database.disconnect();
}

module.exports = app;
module.exports.stop = stop;
