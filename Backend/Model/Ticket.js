const mongoose = require('mongoose');
const { DateTime } = require('luxon');

const Schema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  type: {
    type: String,
    enum: ['bug', 'club-submission'],
    required: true,
  },
  timestamp: {
    type: Date,
    set: (dt) => {
      if (dt instanceof DateTime) {
        return dt.toJSDate();
      }

      return new Date(dt);
    },
    get: (d) => DateTime.fromJSDate(d),
    required: true,
  },
  clubData: {
    type: new mongoose.Schema(
      {
        name: String,
        facebookLink: String,
        instagramLink: String,
        slackLink: String,
        description: String,
        category: String,
        thumbnailUrl: String,
        tags: [{
          type: String,
        }],
      },
      { _id: false },
    ),
    required() { return this.type === 'club-submission'; },
  },
  bugData: {
    type: String,
    required() { return this.type === 'bug'; },
  },
});
exports.Model = mongoose.model('Ticket', Schema);
