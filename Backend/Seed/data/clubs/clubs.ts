import { Types } from 'mongoose';

interface Club {
  name: String;
  facebookLink: string;
  instagramLink: string;
  slackLink: string;
  description: string;
  thumbnailUrl: string;
  category: String;
  admins: Types.ObjectId[];
  _id: Types.ObjectId;
}

const clubsData: Club[] = [
  {
    name: 'Software Engineering Club',
    facebookLink: 'http://facebook.com',
    instagramLink: 'http://instagram.com',
    slackLink: 'http://slack.com',
    description: 'Sharpen your CS technical skills',
    category: 'Computer Science',
    thumbnailUrl: 'https://i.ibb.co/F4rHdKN/sec-club-img.jpg',
    admins: [new Types.ObjectId('16cb91bdc3464f14678934ca'), new Types.ObjectId('26cb91bdc3464f14678934ca')],
    _id: new Types.ObjectId('99cb91bdc3464f14678934ca'),
  },
  {
    name: 'Puppy Club',
    admins: [new Types.ObjectId('27cb91bdc3464f14678934ca')],
    facebookLink: 'http://facebook.com',
    instagramLink: 'http://instagram.com',
    slackLink: 'http://slack.com',
    description: 'We love puppies',
    thumbnailUrl: 'https://scontent-atl3-2.xx.fbcdn.net/v/t1.6435-9/116878441_340562010686784_5214584308875112629_n.jpg?_nc_cat=111&ccb=1-3&_nc_sid=09cbfe&_nc_ohc=jYaqXoW23RsAX_R2VdU&_nc_ht=scontent-atl3-2.xx&oh=372b5aaa58c3530c2b9d569c7fdb9fbc&oe=608FDBDB',
    category: 'Fun',
    _id: new Types.ObjectId('99cd91bdc3464f14678934ca'),
  },
  {
    name: 'ACM',
    admins: [new Types.ObjectId('17cb913dc3464f14678934ca')],
    category: 'Computer Science',
    facebookLink: 'http://facebook.com',
    instagramLink: 'http://instagram.com',
    slackLink: 'http://slack.com',
    description: 'Association for Computing Machinery',
    thumbnailUrl: 'https://media-exp1.licdn.com/dms/image/C4E0BAQE9sv8NfnMF6Q/company-logo_200_200/0/1599034871660?e=2159024400&v=beta&t=nvWPjQvMBcK8zTE1TSjqPTS_zt2ZKqmkco8JG16mE9I',
    _id: new Types.ObjectId('99ce91b5c3464f14678934ca'),
  },
  {
    name: 'Table Tennis Club',
    admins: [new Types.ObjectId('17cb91bdc3464f14678934ca')],
    category: 'Engineering',
    facebookLink: 'http://facebook.com',
    instagramLink: 'http://instagram.com',
    slackLink: 'http://slack.com',
    description: 'Play ping pong every Friday',
    thumbnailUrl: 'https://cdn2.vectorstock.com/i/thumb-large/41/36/two-rackets-for-ping-pong-logo-concept-vector-23464136.jpg',
    _id: new Types.ObjectId('99ce91b4c3464fa4678934ca'),
  },
  {
    name: 'SHPE',
    admins: [new Types.ObjectId('17cb91bdc3464f14678934ca')],
    category: 'Engineering',
    facebookLink: 'http://facebook.com',
    instagramLink: 'http://instagram.com',
    slackLink: 'http://slack.com',
    description: 'Society of Hispanic Professional Engineers',
    thumbnailUrl: 'https://pbs.twimg.com/profile_images/1179189227046756353/LZlRQQZJ_400x400.jpg',
    _id: new Types.ObjectId('99ce7614c3464f14678934ca'),
  },
  {
    name: 'Open Source Club',
    admins: [new Types.ObjectId('17cb91bdc3464f14678934ca')],
    category: 'Sports',
    facebookLink: 'http://facebook.com',
    instagramLink: 'http://instagram.com',
    slackLink: 'http://slack.com',
    description: 'Create open source software',
    thumbnailUrl: 'https://avatars.githubusercontent.com/u/16708767?s=200&v=4',
    _id: new Types.ObjectId('99ce91b233464f14678934ca'),
  },
];

export = clubsData;
