const Ticket = require('../Model/Ticket').Model;
const { NotFoundError } = require('../util/errors/notFoundError');

exports.get = async (id) => {
  const ticket = await Ticket.findById(id);
  if (!ticket) throw new NotFoundError();

  return ticket;
};

exports.create = async (params) => new Ticket(params).save();
