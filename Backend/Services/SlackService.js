const { WebClient } = require('@slack/web-api');
const config = require('../Config/config');
const { ENV } = require('../Config/config');

const { slackToken } = config;
if (!slackToken && ENV !== 'development') {
  console.error('Not in development and slackToken is undefined! Please set your "SLACK_TOKEN" environment variable.');
}
const web = config.slackToken ? new WebClient(config.slackToken) : undefined;

exports.sendBugTicket = async (bug) => {
  if (!web) {
    console.log('Unable to process slack request in staging envirionments');
  } else {
    const text = `*Bug report* \n time: ${bug.timestamp}\nuser: ${bug.user}\ndescription: ${bug.bugData}`;
    this.sendTicket(text);
  }
};

exports.sendServiceTicket = async (isDatabaseRunning) => {
  if (ENV !== 'development') {
    const status = isDatabaseRunning ? 'Server Running' : 'Server Down';
    const text = `*${status}* \ntime: ${new Date().toString()}\nenvironment: ${ENV}`;
    this.sendTicket(text);
  }
};

exports.sendBugTicket = async (bug) => {
  if (!web) {
    console.log('Unable to process slack request in staging envirionments');
  } else {
    const text = `*Bug report* \n time: ${bug.timestamp}\nuser: ${bug.user}\ndescription: ${bug.bugData}`;
    this.sendTicket(text);
  }
};

exports.sendClubTicket = (club) => {
  if (!web) {
    console.log('Unable to process slack request in staging envirionments');
  } else {
    const { clubData } = club;
    let clubSubmission = '*Club submission request* \n';
    Object.keys(clubData.toJSON()).forEach((key) => {
      clubSubmission += `\t${key}: ${clubData[key]}\n`;
    });
    this.sendTicket(clubSubmission);
  }
};

exports.sendTicket = async (text) => {
  // if (!web) {
  //   console.error('Failed to send ticket because no Slack Web Client was created. Is the "SLACK_TOKEN" environment variable set?');
  //   return;
  // }

  // try {
  //   await web.chat.postMessage({
  //     channel: '#clubfinity-tickets',
  //     text,
  //   });
  // } catch (error) {
  //   console.log(error);
  // }
};
