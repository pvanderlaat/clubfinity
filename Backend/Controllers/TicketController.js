const { validationResult, body } = require('express-validator');
const { DateTime } = require('luxon');
const ticketDAO = require('../DAO/TicketDAO');
const { ValidationError } = require('../util/errors/validationError');
const { catchErrors } = require('../util/httpUtil');
const { sendBugTicket, sendClubTicket } = require('../Services/SlackService');

const validateTicketData = (req) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) throw new ValidationError(errors.array());
};

exports.create = async (req, res) => catchErrors(res, async () => {
  validateTicketData(req);

  const { type, clubData, bugData } = req.body;
  const timestamp = DateTime.fromJSDate(new Date());
  const user = req.userId;

  switch (type) {
    case 'bug':
      sendBugTicket(await ticketDAO.create({
        type, bugData, timestamp, user,
      }));
      return true;
    case 'club-submission':
      sendClubTicket(await ticketDAO.create({
        type, clubData, timestamp, user,
      }));
      return true;
    default:
      throw new Error(`Invalid type ${type}`);
  }
});

exports.validate = (type) => {
  switch (type) {
    case 'validateTicketInfo': {
      return [
        body('type', 'Type is not valid')
          .exists()
          .custom((value) => value === 'bug' || value === 'club-submission'),
        body('bugData')
          .if((_, { req }) => req.body.type === 'bug')
          .exists(),
        body('clubData')
          .if((_, { req }) => req.body.type === 'club-submission')
          .exists()
          .custom((value) => {
            if (typeof (value) !== 'object') {
              return false;
            }
            const fields = Object.getOwnPropertyNames(value);
            return fields.includes('name') && fields.includes('description');
          }),
      ];
    }
    default: {
      throw new Error('Invalid validator');
    }
  }
};
