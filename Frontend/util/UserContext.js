import React from 'react';

const UserContext = React.createContext({
  user: {},
  setUser: () => {},
  message: '',
  setMessage: () => '',
});

export default UserContext;
