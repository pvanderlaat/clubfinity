import { DateTime } from 'luxon';
import { Platform } from 'react-native';

export const DATE_PICKER_FORMAT = 'MMM D YYYY';
export const TIME_PICKER_FORMAT = 'hh:mm A';

export function combineAndParseDateTime(date, time) {
  const combined = `${date} ${time}`;
  return DateTime.fromFormat(combined, 'MMM d yyyy hh:mm a');
}

export function extractDateAndTime(dateTime) {
  return { date: dateTime.toFormat(DATE_PICKER_FORMAT), time: dateTime.toFormat(TIME_PICKER_FORMAT) };
}

export function formatToMonthAndDay(date) {
  const dateTime = DateTime.fromISO(date);
  // toLocaleString is buggy on android
  if (Platform.OS === 'android') {
    const dateWithYear = dateTime.toLocaleString(DateTime.DATE_MED);
    return dateWithYear.substr(0, dateWithYear.length - 6);
  }
  return dateTime.toLocaleString({ month: 'short', day: 'numeric' });
}

export function formatToTime(date) {
  return DateTime.fromISO(date).toLocaleString(DateTime.TIME_SIMPLE);
}

export function today() {
  return DateTime.local();
}

export function hoursUntil(date) {
  return date.diff(today(), ['years', 'months', 'days', 'hours']).hours;
}

export function daysUntil(date) {
  return date.diff(today(), ['years', 'months', 'days']).days;
}
