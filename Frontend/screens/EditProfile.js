import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import Majors from '../data/Majors';
import ClassYears from '../data/ClassYears';
import UserContext from '../util/UserContext';
import UserApi from '../api/UserApi';
import Form from '../components/Form';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';

export default class EditProfile extends Component {
  static contextType = UserContext;

  nameRegex = /^[a-zA-Z()]+$/;

  validator = new Validator({
    firstName: [Validator.required(), Validator.regex(this.nameRegex)],
    lastName: [Validator.required(), Validator.regex(this.nameRegex)],
    major: [Validator.required()],
    classYear: [Validator.required(), Validator.numeric()],
  });

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      major: '',
      classYear: '',
      processingRequest: { status: false, message: '' },
      errors: {},
    };
  }

  componentDidMount() {
    const { user } = this.context;

    this.setState({
      firstName: user.name.first,
      lastName: user.name.last,
      major: user.major,
      classYear: user.year.toString(),
    });
  }

  editProfile = async () => {
    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({
        processingRequest: { status: false, message: '' },
        errors: validationResults.errors,
      });

      return;
    }

    this.setState({
      processingRequest: { status: true, message: 'Updating...' },
    });

    const {
      firstName, lastName, major, classYear,
    } = this.state;
    const { setUser, setMessage } = this.context;
    const updateUserResponse = await UserApi.updateUser({
      major,
      year: classYear,
      name: { first: firstName, last: lastName },
    });

    if (updateUserResponse.error) {
      alert('Unable to update user');
      setMessage('Unable to update user.');
      console.log(updateUserResponse.error);
      return;
    }

    setMessage('Profile updated.');

    this.setState({
      processingRequest: { status: true, message: 'Updated your profile!' },
    });

    setUser(updateUserResponse.data);
  };

  render() {
    const {
      processingRequest, errors, firstName, lastName, major, classYear,
    } = this.state;

    return (
      <Container>
        <Content>
          <Form>
            <Form.Text
              placeholder="First name"
              value={firstName}
              onChange={(value) => updateStateAndClearErrors(this, 'firstName', value)}
              error={errors.firstName}
            />
            <Form.Text
              placeholder="Last name"
              value={lastName}
              onChange={(value) => updateStateAndClearErrors(this, 'lastName', value)}
              error={errors.lastName}
            />
            <Form.Select
              placeholder="Major"
              options={Majors}
              value={major}
              onChange={(value) => updateStateAndClearErrors(this, 'major', value)}
              error={errors.major}
            />
            <Form.Select
              placeholder="Year"
              options={ClassYears}
              value={classYear}
              onChange={(value) => updateStateAndClearErrors(this, 'classYear', value)}
              error={errors.classYear}
            />
            <Form.Button
              text={
                processingRequest.status
                  ? processingRequest.message
                  : 'Update Profile'
              }
              onPress={this.editProfile}
            />
          </Form>
        </Content>
      </Container>
    );
  }
}
