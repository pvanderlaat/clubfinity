import React, { useContext } from 'react';
import { AsyncStorage } from 'react-native';
import {
  Container, Content, List,
} from 'native-base';
import SettingsListItem from '../components/SettingsListItem';
import UserContext from '../util/UserContext';

import { resetRoot } from '../navigation/helpers/resetNavigationHelper';

const SettingScr = (props) => {
  const userContext = useContext(UserContext);

  const signOut = async () => {
    await AsyncStorage.removeItem('userToken');
    const { setUser } = userContext;

    resetRoot();
    setUser(null);
  };

  const { navigation } = props;
  return (
    <Container>
      <Content>
        <List>
          <SettingsListItem
            onPress={() => navigation.navigate('EditProfileScr')}
            icon="md-person"
            label="Edit Profile"
          />
          <SettingsListItem
            onPress={() => navigation.navigate('SubmitClubScr')}
            icon="md-create"
            label="Submit a Club"
          />
          <SettingsListItem
            onPress={() => navigation.navigate('EditNotificationsScr')}
            icon="md-notifications"
            label="Notifications"
          />
          <SettingsListItem
            onPress={() => navigation.navigate('ReportBugScr')}
            icon="md-bug"
            label="Report a Bug"
          />
          <SettingsListItem
            onPress={() => signOut()}
            icon="md-log-out"
            label="Logout"
          />
        </List>
      </Content>
    </Container>
  );
};

export default SettingScr;
