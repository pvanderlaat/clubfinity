import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import Form from '../components/Form';
import TicketsApi from '../api/TicketsApi';
import Validator from '../components/form/validation/Validator';
import { updateStateAndClearErrors } from '../util/formUtil';
import UserContext from '../util/UserContext';

const clubCategories = [
  { label: 'Computer Science', value: 'Computer Science' },
  { label: 'Research', value: 'Research' },
  { label: 'Business', value: 'Business' },
  { label: 'Arts', value: 'Arts' },
  { label: 'Engineering', value: 'Engineering' },
  { label: 'Health', value: 'Health' },
  { label: 'Journalism', value: 'Journalism' },
  { label: 'Liberal Arts', value: 'Liberal Arts' },
  { label: 'Cultural', value: 'Cultural' },
  { label: 'Honor Society', value: 'Honor Society' },
  { label: 'Media', value: 'Media' },
  { label: 'Professional/Career', value: 'Professional/Career' },
  { label: 'Religious/Spiritual', value: 'Religious/Spiritual' },
  { label: 'Sport Clubs', value: 'Sport Clubs' },
  { label: 'Student Government', value: 'Student Government' },
];

const positions = [
  { label: 'President', value: 'President' },
  { label: 'Vice President', value: 'Vice President' },
  { label: 'Treasurer', value: 'Treasurer' },
  { label: 'Other', value: 'Other' },
];

export default class SubmitClubScr extends Component {
  static contextType = UserContext;

  validator = new Validator({
    clubName: [Validator.required(), Validator.minLength(3)],
    clubDescription: [Validator.required(), Validator.maxLength(280)],
    clubCategory: [Validator.required()],
    position: [Validator.required()],
    thumbnailUrl: [Validator.required(), Validator.validUrl()],
    facebookLink: [Validator.required(), Validator.validFacebookUrl()],
    instagramLink: [Validator.required(), Validator.validInstagramUrl()],
    slackLink: [Validator.required(), Validator.validSlackUrl()],
  });

  constructor(props) {
    super(props);
    this.state = {
      clubName: '',
      clubDescription: '',
      clubCategory: '',
      position: '',
      tags: 'placeholder',
      thumbnailUrl: '',
      facebookLink: '',
      instagramLink: '',
      slackLink: '',
      processingRequest: false,
      errors: {},
    };
  }

  submitClub = async () => {
    const {
      clubName,
      clubCategory,
      clubDescription,
      tags,
      thumbnailUrl,
      facebookLink,
      instagramLink,
      slackLink,
    } = this.state;

    const { navigation } = this.props;
    const { setMessage } = this.context;

    const validationResults = this.validator.validate(this.state);

    if (!validationResults.valid) {
      this.setState({
        errors: validationResults.errors,
      });
      return;
    }

    this.setState({ processingRequest: true });

    const submitClubResponse = await TicketsApi.submitClub(
      clubName,
      clubCategory,
      clubDescription,
      tags,
      thumbnailUrl,
      facebookLink,
      instagramLink,
      slackLink,
    );
    if (submitClubResponse) {
      this.setState({ processingRequest: false });
      setMessage('Club submitted.');
      navigation.goBack();
    } else {
      setMessage('Could not submit club.');
      this.setState({ processingRequest: false });
    }
  };

  render() {
    const {
      errors,
      processingRequest,
      position,
      clubCategory,
      clubDescription,
      facebookLink,
      instagramLink,
      slackLink,
      clubName,
      thumbnailUrl,
    } = this.state;

    return (
      <Container>
        <Content>
          <Form>
            <Form.Text
              value={clubName}
              onChange={(value) => updateStateAndClearErrors(this, 'clubName', value)}
              placeholder="Name"
              error={errors.clubName}
            />
            <Form.Select
              value={position}
              onChange={(value) => updateStateAndClearErrors(this, 'position', value)}
              options={positions}
              placeholder="Position"
              error={errors.position}
            />
            <Form.Select
              value={clubCategory}
              onChange={(value) => updateStateAndClearErrors(this, 'clubCategory', value)}
              options={clubCategories}
              placeholder="Category"
              error={errors.clubCategory}
            />
            <Form.Text
              value={thumbnailUrl}
              onChange={(value) => updateStateAndClearErrors(this, 'thumbnailUrl', value)}
              placeholder="Thumbnail URL"
              error={errors.thumbnailUrl}
            />
            <Form.Text
              value={facebookLink}
              onChange={(value) => updateStateAndClearErrors(this, 'facebookLink', value)}
              placeholder="Facebook"
              error={errors.facebookLink}
            />
            <Form.Text
              value={instagramLink}
              onChange={(value) => updateStateAndClearErrors(this, 'instagramLink', value)}
              placeholder="Instagram"
              error={errors.instagramLink}
            />
            <Form.Text
              value={slackLink}
              onChange={(value) => updateStateAndClearErrors(this, 'slackLink', value)}
              placeholder="Slack"
              error={errors.slackLink}
            />
            <Form.TextArea
              value={clubDescription}
              onChange={(value) => updateStateAndClearErrors(this, 'clubDescription', value)}
              placeholder="Description"
              error={errors.clubDescription}
            />
            <Form.Button
              text={processingRequest ? 'Submitting...' : 'Submit Club'}
              onPress={this.submitClub}
            />
          </Form>
        </Content>
      </Container>
    );
  }
}
