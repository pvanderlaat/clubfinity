import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Container, Content, H1, StyleProvider, Thumbnail,
} from 'native-base';
import getTheme from '../native-base-theme/components';
import thumbnailTheme from '../native-base-theme/components/Thumbnail';
import UserContext from '../util/UserContext';
import ClubsApi from '../api/ClubsApi';
import Form from '../components/Form';
import { updateStateAndClearErrors } from '../util/formUtil';
import Validator from '../components/form/validation/Validator';

export default class EditClub extends Component {
  static contextType = UserContext;

  validator = new Validator({
    name: [Validator.required(), Validator.minLength(3)],
    description: [Validator.required(), Validator.maxLength(280)],
    thumbnailUrl: [Validator.required(), Validator.validUrl()],
    facebookLink: [Validator.required(), Validator.validFacebookUrl()],
    instagramLink: [Validator.required(), Validator.validInstagramUrl()],
    slackLink: [Validator.required(), Validator.validSlackUrl()],
  });

  constructor(props) {
    super(props);

    this.state = {
      name: '',
      description: '',
      thumbnailUrl: '',
      facebookLink: '',
      instagramLink: '',
      slackLink: '',
      processingRequest: false,
      errors: {},
    };
  }

  componentDidMount() {
    const { route } = this.props;
    const { club } = route.params;
    this.setState({
      name: club.name,
      description: club.description,
      thumbnailUrl: club.thumbnailUrl,
      facebookLink: club.facebookLink,
      slackLink: club.slackLink,
      instagramLink: club.instagramLink,
    });
  }

  editClub = async () => {
    const validationResults = this.validator.validate(this.state);
    if (!validationResults.valid) {
      this.setState({
        processingRequest: false,
        errors: validationResults.errors,
      });

      return;
    }
    this.setState({ processingRequest: true });

    const { route, navigation } = this.props;
    const { club } = route.params;
    const { setMessage } = this.context;

    const {
      name, description, slackLink, facebookLink, instagramLink,
    } = this.state;

    const updatedClubValues = {
      ...club,
      name,
      description,
      slackLink,
      facebookLink,
      instagramLink,
    };

    const editedClubResponse = await ClubsApi.updateClub(
      club._id,
      updatedClubValues,
    );
    if (editedClubResponse.successfulRequest) {
      this.setState({ processingRequest: false });
      setMessage('Club updated.');
      navigation.pop();
    } else {
      setMessage('Unable to update club.');
      console.log(editedClubResponse.error);
      this.setState({ processingRequest: false });
    }
  };

  render() {
    const {
      name,
      description,
      thumbnailUrl,
      slackLink,
      facebookLink,
      instagramLink,
      errors,
      processingRequest,
    } = this.state;

    return (
      <Container>
        <Content>
          <Form>
            <View style={{ display: 'flex', alignItems: 'center' }}>
              <View style={{ paddingTop: '10%' }}>
                <StyleProvider style={getTheme(thumbnailTheme)}>
                  <Thumbnail source={{ uri: thumbnailUrl }} large />
                </StyleProvider>
              </View>
              <H1 style={{ paddingBottom: '2%', paddingTop: '5%' }}>{name}</H1>
            </View>
            <Form.Text
              placeholder="Thumbnail URL"
              value={thumbnailUrl}
              onChange={(value) => updateStateAndClearErrors(this, 'thumbnailUrl', value)}
              error={errors.thumbnailUrl}
            />
            <Form.TextArea
              placeholder="Description"
              value={description}
              onChange={(value) => updateStateAndClearErrors(this, 'description', value)}
              error={errors.description}
            />
            <Form.Text
              placeholder="Facebook"
              value={facebookLink}
              onChange={(value) => updateStateAndClearErrors(this, 'facebookLink', value)}
              error={errors.facebookLink}
            />
            <Form.Text
              placeholder="Instagram"
              value={instagramLink}
              onChange={(value) => updateStateAndClearErrors(this, 'instagramLink', value)}
              error={errors.instagramLink}
            />
            <Form.Text
              placeholder="Slack"
              value={slackLink}
              onChange={(value) => updateStateAndClearErrors(this, 'slackLink', value)}
              error={errors.slackLink}
            />
            <Form.Button
              text={processingRequest ? 'Editing...' : 'Save'}
              onPress={this.editClub}
            />
          </Form>
        </Content>
      </Container>
    );
  }
}
