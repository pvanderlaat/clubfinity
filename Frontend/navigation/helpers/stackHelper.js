import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import CalendarScr from '../../screens/CalendarScr';
import HomeScr from '../../screens/HomeScr';
import DiscoverScr from '../../screens/DiscoverScr';
import ProfileScr from '../../screens/ProfileScr';
import ClubScr from '../../screens/ClubScr';
import EditProfile from '../../screens/EditProfile';
import EditNotificationsScr from '../../screens/EditNotificationsScr';
import SubmitClubScr from '../../screens/SubmitClubScr';
import SettingScr from '../../screens/SettingScr';
import EventList from '../../screens/EventList';
import AnnouncementList from '../../screens/AnnouncementList';
import EventCreation from '../../screens/EventCreation';
import CreateAnnouncementScr from '../../screens/CreateAnnouncementScr';
import AdminList from '../../screens/AdminList';
import EditClub from '../../screens/EditClub';
import EditEvent from '../../screens/EditEvent';
import EventScr from '../../screens/EventScr';
import AnnouncementScr from '../../screens/AnnouncementScr';
import EditAnnouncement from '../../screens/EditAnnouncements';
import ReportBugScr from '../../screens/ReportBugScr';

const EventPages = {
  EventScr,
  EditEvent,
};

const ClubPages = {
  ClubScr,
  EditClub,
  CreateAnnouncementScr,
  EventCreation,
  EventList,
  AnnouncementList,
  AnnouncementScr,
  EditAnnouncement,
  AdminList,
};

const ProfilePages = {
  EditProfileScr: EditProfile,
  EditNotificationsScr,
  Settings: SettingScr,
  SubmitClubScr,
  ReportBugScr,
};

const getStackPages = (pageName) => {
  if (pageName === 'Profile') {
    return {
      Profile: ProfileScr,
      ...Object.assign(ProfilePages),
      ...Object.assign(ClubPages),
      ...Object.assign(EventPages),
    };
  } if (pageName === 'Home') {
    return {
      Home: HomeScr,
      ...Object.assign(EventPages),
    };
  } if (pageName === 'Discover') {
    return {
      Discover: DiscoverScr,
      ...Object.assign(ClubPages),
      ...Object.assign(EventPages),
    };
  } if (pageName === 'Calendar') {
    return {
      Calendar: CalendarScr,
      ...Object.assign(EventPages),
    };
  }
  return Error('Error loading navigation frames.');
};

const ICON_SIZE = 30;
const ICON_COLOR = 'white';

const getHeaderRight = (pageName, navigation) => (pageName === 'Profile' ? {
  headerRight: () => <HeaderIconComponent navigation={navigation} />,
} : {});

const HeaderIconComponent = (props) => {
  const { navigation } = props;
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
      <TouchableOpacity
        onPress={() => navigation.navigate('Settings')}
        style={{
          paddingTop: '2%',
          paddingRight: '2%',
          alignSelf: 'flex-end',
        }}
      >
        <Ionicons name="md-settings" size={ICON_SIZE} color={ICON_COLOR} />
      </TouchableOpacity>
    </View>
  );
};

const generateStack = (Stack, pageName) => {
  const stackPages = getStackPages(pageName);
  return Object.keys(stackPages).map(
    (name) => (
      <Stack.Screen
        name={name}
        options={({ navigation }) => (getHeaderRight(name, navigation))}
        component={stackPages[name]}
      />
    ),
  );
};

export default generateStack;
