import { createNavigationContainerRef } from '@react-navigation/native';

export const navigationRef = createNavigationContainerRef();

export const resetRoot = () => {
  navigationRef.reset({
    index: 0,
    routes: [{ name: 'HomeStack' }],
  });
};
