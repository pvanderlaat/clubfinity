import React, { useContext } from 'react';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Ionicons } from '@expo/vector-icons';
import UserContext from '../util/UserContext';

import SignupScr from '../screens/SignupScr';
import SigninScr from '../screens/SigninScr';
import EmailVerificationScr from '../screens/EmailVerificationScr';
import ClubfinityStack from './helpers/ClubfinityStack';
import { navigationRef } from './helpers/resetNavigationHelper';

const AuthStack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const getOptions = (barLabel, iconName) => ({
  tabBarIcon: ({ color, size }) => <Ionicons name={iconName} size={size} color={color} />,
  tabBarLabel: barLabel,
});

const getTabScreenForStack = (stackName, iconName) => (
  <Tab.Screen
    options={getOptions(stackName, iconName)}
    name={`${stackName}Stack`}
    children={() => <ClubfinityStack name={stackName} />}
  />
);

export default function AppNavigator() {
  const userContext = useContext(UserContext);
  const { user } = userContext;

  const MyTheme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'rgb(255, 255, 255)',
    },
  };

  return (
    <NavigationContainer theme={MyTheme} ref={navigationRef}>
      {user === null ? (
        <AuthStack.Navigator
          initialRouteName="SignIn"
          screenOptions={{
            headerShown: false,
          }}
        >
          <AuthStack.Screen name="SignIn" component={SigninScr} />
          <AuthStack.Screen name="SignUp" component={SignupScr} />
          <AuthStack.Screen
            name="EmailVerification"
            component={EmailVerificationScr}
          />
        </AuthStack.Navigator>
      ) : (
        <Tab.Navigator
          initialRouteName="HomeStack"
          screenOptions={{
            headerShown: false,
          }}
        >
          {getTabScreenForStack('Home', 'md-home')}
          {getTabScreenForStack('Discover', 'md-search')}
          {getTabScreenForStack('Calendar', 'md-calendar')}
          {getTabScreenForStack('Profile', 'md-happy')}
        </Tab.Navigator>
      )}
    </NavigationContainer>
  );
}
