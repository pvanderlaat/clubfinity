// eslint-disable-next-line no-undef
const env = __DEV__ ? 'development' : 'production';

const config = {
  development: {
    url: 'http://clubfinity.staging.pvanderlaat.com:8080/',
  },
  production: {
    url: 'http://clubfinity.prod.pvanderlaat.com:8080/',
  },
};

export default config[env];
