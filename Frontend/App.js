import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  Platform,
  StatusBar,
  LogBox,
} from 'react-native';

import * as Font from 'expo-font';
import Roboto from 'native-base/Fonts/Roboto.ttf';
// eslint-disable-next-line camelcase
import Roboto_medium from 'native-base/Fonts/Roboto_medium.ttf';
import Ionicons from 'native-base/Fonts/Ionicons.ttf';
import UserContext from './util/UserContext';
import AppNavigator from './navigation/AppNavigator';
import Snackbar from './components/Snackbar';

LogBox.ignoreAllLogs(true);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoadingComplete: false,
      user: null,
      message: '',
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto,
      Roboto_medium,
      Ionicons,
      ...Ionicons.font,
    });
    this.setState({ isLoadingComplete: true });
  }

  setUser = (newUser) => {
    this.setState({
      user: newUser,
    });
  };

  setMessage = (newMessage) => {
    // Forces re-render snackbar even when the message stays the same.
    this.setState({
      message: '',
    });
    this.setState({
      message: newMessage,
    });
  };

  render() {
    const { user, message, isLoadingComplete } = this.state;
    const { setUser, setMessage } = this;
    if (!isLoadingComplete) {
      return (
        <View style={{ flex: 1, padding: 20, justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <UserContext.Provider
          value={{
            user,
            setUser,
            message,
            setMessage,
          }}
        >
          <AppNavigator />
          <Snackbar />
        </UserContext.Provider>
      </>
    );
  }
}
