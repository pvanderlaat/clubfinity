/* eslint-disable react/jsx-props-no-spreading,react/prop-types */

import React, { useRef } from 'react';
import { View, StyleSheet } from 'react-native';
import TextInput from './form/TextInput';
import SelectField from './form/SelectInput';
import TextAreaInput from './form/TextAreaInput';
import Button from './form/Button';
import DateInput from './form/date/DateInput';

const styles = StyleSheet.create({
  container: {
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop: 10,
  },
  item: {
    marginTop: 11,
    marginBottom: 11,
  },
});

export default class Form extends React.Component {
  render() {
    return (
      <View style={styles.container} ref={(c) => this._root = c} {...this.props} />
    );
  }
}

Form.Text = (props) => {
  const { style, ...rest } = props;

  return (
    <TextInput style={{ ...styles.item, ...style }} {...rest} />
  );
};
Form.TextArea = (props) => {
  const RichText = useRef();
  // function editorInitializedCallback() {

  //   RichText.current?.registerToolbar(() => {
  //     // items contain all the actions that are currently active
  //     console.log(
  //       'Toolbar click, selected items (insert end callback):',
  //     );
  //   });
  // }

  const { style, ...rest } = props;
  // const RichText = useRef(); //reference to the RichEditor component
  // const [article, setArticle] = useState("");

  return (
    <TextAreaInput
      style={{ ...styles.item, ...style }}
      RichText={RichText}
      // editorInitializedCallback={editorInitializedCallback}
      {...rest}
    />
  );
};
// Form.TextArea = (props) => {
//   const { style, ...rest } = props;

//   return (
//     <TextAreaInput style={{ ...styles.item, ...style }} {...rest} />
//   );
// };

Form.Select = (props) => {
  const { style, ...rest } = props;

  return (
    <SelectField style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.Date = (props) => {
  const { style, ...rest } = props;

  return (
    <DateInput style={{ ...styles.item, ...style }} {...rest} />
  );
};

Form.Button = (props) => {
  const { style, ...rest } = props;

  return (
    <Button style={{ ...styles.item, ...style }} {...rest} />
  );
};
