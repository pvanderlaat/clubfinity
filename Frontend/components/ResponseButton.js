import React, { Component } from 'react';
import { Ionicons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import { View, Text, Button } from 'native-base';
import { StyleSheet } from 'react-native';
import standardColors from '../constants/Colors';

const style = StyleSheet.create({
  responseButtonWrapper: {
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'center',
  },
  selected: {
    backgroundColor: standardColors.blueFade,
  },
  unselected: {
    backgroundColor: standardColors.grayFade,
  },
  button: {
    height: 50,
    width: 50,
    borderRadius: 25,
    justifyContent: 'center',
    shadowOffset: { height: 0, width: 0 },
    shadowOpacity: 0,
    elevation: 0,
  },
});

export default class ResponseButton extends Component {
  static propTypes = {
    clickHandler: PropTypes.func.isRequired,
    selected: PropTypes.bool.isRequired,
    icon: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
  };

  render() {
    const {
      selected, clickHandler, icon, label,
    } = this.props;
    return (
      <View style={style.responseButtonWrapper}>
        <View>
          <Button
            onPress={clickHandler}
            style={[style.button, selected ? style.selected : style.unselected]}
          >
            <Ionicons
              color={selected ? standardColors.blue : standardColors.gray}
              name={icon}
              size={25}
            />
          </Button>
        </View>
        <Text style={{ fontSize: 13 }}>
          {label}
        </Text>
      </View>
    );
  }
}
