import React from 'react';
import {
  View, StyleSheet, Platform, ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';
import IOSDateInput from './IOSDateInput';
import AndroidDateInput from './AndroidDateInput';
import LabelAndErrorContainer from '../LabelAndErrorContainer';
import colors from '../../../util/colors';

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  androidContainerError: {
    borderColor: colors.error,
  },
});

export default class DateInput extends React.Component {
  static propTypes = {
    value: PropTypes.instanceOf(DateTime).isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    error: PropTypes.string,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    placeholder: 'Date and Time',
    label: null,
    error: null,
    style: {},
  }

  render() {
    const {
      value, onChange, placeholder, label, error, style,
    } = this.props;

    // If a string is accidentally passed as the value
    // convert to a DateTime to avoid later errors
    const convertedValue = typeof value === 'string' ? DateTime.fromISO(value) : value;

    return (
      <LabelAndErrorContainer
        label={label}
        error={error}
        style={style}
      >
        <View style={styles.container}>
          {/* Two separate components are used since date pickers on android */}
          {/* use pop-up models, where on IOS they use always visible inputs */}
          {/* Therefore each manages state differently */}
          {Platform.OS === 'ios'
            ? (
              <IOSDateInput
                value={convertedValue}
                onChange={onChange}
              />
            )
            : (
              <AndroidDateInput
                value={convertedValue}
                onChange={onChange}
                placeholder={placeholder}
                style={error ? styles.androidContainerError : {}}
              />
            )}
        </View>
      </LabelAndErrorContainer>
    );
  }
}
