import React from 'react';
import PropTypes from 'prop-types';
import { DateTime } from 'luxon';
import {
  Text, StyleSheet, TouchableOpacity, ViewPropTypes,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { Ionicons } from '@expo/vector-icons';
import { DATE_PICKER_FORMAT, TIME_PICKER_FORMAT } from '../../../util/dateUtil';
import colors from '../../../util/colors';

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',

    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'transparent',

    backgroundColor: colors.grayScale1,
    padding: 13,
    paddingLeft: 20,
    width: '100%',
  },
  timePicker: {
    width: 100,
    backgroundColor: colors.grayScale1,
  },
  displayText: {
    color: colors.inputText,
    fontSize: 16,
  },
  placeholderText: {
    color: colors.inputPlaceholder,
  },
  datePicker: {
    width: '100%',
    padding: 20,
    fontSize: 24,
  },
});

export default class AndroidDateInput extends React.Component {
  static propTypes = {
    value: PropTypes.instanceOf(DateTime).isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string.isRequired,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    style: {},
  }

  constructor(props) {
    super(props);

    this.state = {
      showDatePicker: false,
      showTimePicker: false,
    };
  }

  get displayValue() {
    const { value, placeholder } = this.props;

    // See https://gist.github.com/ezirmusitua/3b1797c250cc9de4f71a037eb6160966 for possible format information
    return value?.toFormat('ff') ?? placeholder;
  }

  openPicker = () => {
    const { onChange, value } = this.props;

    // The date picker does not accept null values, so as soon as the picker is edited,
    // set the value to be the current date
    if (!value) {
      onChange(DateTime.local().startOf('minute'));
    }

    this.setState({
      showDatePicker: true,
    });
  }

  onDateChange = (event, date) => {
    const { value, onChange } = this.props;

    if (!date) {
      this.setState({ showDatePicker: false });

      return;
    }

    const dateTime = DateTime.fromJSDate(date);

    const newDate = value.set({
      year: dateTime.year,
      month: dateTime.month,
      day: dateTime.day,
    });

    this.setState({
      showDatePicker: false,
      showTimePicker: true,
    });

    onChange(newDate);
  }

  onTimeChange = (event, date) => {
    const { value, onChange } = this.props;

    if (!date) {
      this.setState({ showTimePicker: false });

      return;
    }

    const dateTime = DateTime.fromJSDate(date);

    const newDate = value.set({
      hour: dateTime.hour,
      minute: dateTime.minute,
    }).startOf('minute');

    this.setState({
      showTimePicker: false,
    });

    onChange(newDate);
  }

  render() {
    const { value, style } = this.props;
    const { showDatePicker, showTimePicker } = this.state;

    return (
      <>
        <TouchableOpacity style={{ ...styles.container, ...style }} onPress={this.openPicker}>
          <Text style={{ ...styles.displayText, ...(!value ? styles.placeholderText : {}) }}>
            {this.displayValue}
          </Text>
          <Ionicons
            name="chevron-down-outline"
            size={20}
            style={styles.dropdownIcon}
            color={colors.inputPlaceholder}
          />
        </TouchableOpacity>
        {showDatePicker && (
          <DateTimePicker
            mode="date"
            style={styles.datePicker}
            value={value.toJSDate()}
            format={DATE_PICKER_FORMAT}
            onChange={this.onDateChange}
          />
        )}
        {showTimePicker && (
          <DateTimePicker
            mode="time"
            value={value.toJSDate()}
            format={TIME_PICKER_FORMAT}
            style={styles.timePicker}
            onChange={this.onTimeChange}
          />
        )}
      </>
    );
  }
}
