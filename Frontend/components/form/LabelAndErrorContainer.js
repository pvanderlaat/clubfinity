import React from 'react';
import {
  View, Text, StyleSheet, ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import colors from '../../util/colors';

const styles = StyleSheet.create({
  container: {
    position: 'relative',
  },
  label: {
    marginBottom: 5,
    fontSize: 16,
  },
  errorMessage: {
    position: 'absolute',
    right: 0,
    bottom: -16,
    color: colors.error,
    fontSize: 12,
  },
});

export default class extends React.Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    style: ViewPropTypes.style,
    label: PropTypes.string,
    error: PropTypes.string,
  };

  static defaultProps = {
    style: {},
    label: null,
    error: null,
  };

  render() {
    const {
      label, error, style, children,
    } = this.props;

    return (
      <View style={{ ...styles.container, ...style }}>
        {label && (
          <Text style={styles.label}>{label}</Text>
        )}
        {children}
        {error && (
          <Text style={styles.errorMessage}>{error}</Text>
        )}
      </View>
    );
  }
}
