import React from 'react';
import {
  Platform, StyleSheet, ViewPropTypes, View,
} from 'react-native';
import {
  Picker,
} from 'native-base';
import PropTypes from 'prop-types';
import { Ionicons } from '@expo/vector-icons';
import colors from '../../util/colors';
import LabelAndErrorContainer from './LabelAndErrorContainer';

const styles = StyleSheet.create({
  selectInput: {
    // This is required to avoid content jump when an error is added
    borderWidth: 1,
    borderColor: 'transparent',

    borderRadius: 10,
    backgroundColor: colors.grayScale1,
    paddingLeft: 5,
    width: '100%',
    color: colors.inputText,
  },
  androidSelectContainer: {
    borderWidth: 1,
    borderColor: 'transparent',

    borderRadius: 10,
    backgroundColor: colors.grayScale1,
    paddingLeft: 12,
    width: '100%',
    color: colors.inputText,
  },
  errorInput: {
    borderColor: colors.error,
  },
  selectInputText: {
    color: colors.inputText,
  },
  selectInputPlaceholder: {
    color: colors.inputPlaceholder,
  },
  dropdownIcon: {
    paddingRight: '5%',
  },
});

export default class extends React.Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    options: PropTypes.array.isRequired,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    style: ViewPropTypes.style,
    error: PropTypes.string,
  };

  static defaultProps = {
    placeholder: null,
    label: null,
    style: {},
    error: null,
  };

  renderAndroidPicker() {
    const {
      options, value, onChange, placeholder, error,
    } = this.props;

    const items = options.map((s) => (
      <Picker.Item
        value={s.value}
        label={s.label}
        key={s.label}
      />
    ));

    // Since android always defaults to the first option in a dropdown, the placeholder must be added as an option
    // The given color allows it to appear as a placeholder with a lighter text color
    if (placeholder) {
      items.unshift(
        <Picker.Item
          value=""
          label={placeholder}
          key={placeholder}
          color={colors.inputPlaceholder}
        />,
      );
    }

    // On Android, the picker must be wrapped in a view to give it a custom style since the picker style cannot
    // easily be changed and doing so hides the dropdown arrow icon
    return (
      <View style={{ ...styles.androidSelectContainer, ...(error ? styles.errorInput : {}) }}>
        <Picker
          mode="dropdown"
          selectedValue={value}
          onValueChange={(newValue) => onChange(newValue)}
        >
          {items}
        </Picker>
      </View>
    );
  }

  renderIOSPicker() {
    const {
      options, value, onChange, placeholder, error,
    } = this.props;

    const pickerItems = options.map((s) => (
      <Picker.Item
        value={s.value}
        label={s.label}
        key={s.label}
      />
    ));

    return (
      <Picker
        mode="dropdown"
        placeholder={placeholder}
        selectedValue={value}
        onValueChange={(newValue) => onChange(newValue)}
        style={{ ...styles.selectInput, ...(error ? styles.errorInput : {}) }}
        placeholderStyle={styles.selectInputPlaceholder}
        textStyle={styles.selectInputText}
        color={colors.inputPlaceholder}
        iosIcon={(
          <Ionicons
            name="chevron-down-outline"
            size={20}
            style={styles.dropdownIcon}
            color={colors.inputPlaceholder}
          />
        )}
      >
        {pickerItems}
      </Picker>
    );
  }

  renderPicker() {
    return Platform.OS === 'ios' ? this.renderIOSPicker() : this.renderAndroidPicker();
  }

  render() {
    const {
      label, style, error,
    } = this.props;

    return (
      <LabelAndErrorContainer
        style={style}
        label={label}
        error={error}
      >
        {this.renderPicker()}
      </LabelAndErrorContainer>
    );
  }
}
