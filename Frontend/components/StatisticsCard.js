import React, { Component } from 'react';
import {
  Text,
  View,
} from 'native-base';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
  card: {
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
    paddingLeft: 8,
    paddingRight: 8,
    paddingTop: 20,
    paddingBottom: 20,
    flexGrow: 0,
    marginRight: '1%',
    marginLeft: '1%',
    maxWidth: '33.33333%',
    flexBasis: '31.333333%',
  },
  label: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
  },
  number: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default class StatisticsCard extends Component {
  static propTypes = {
    number: PropTypes.number.isRequired,
    label: PropTypes.string.isRequired,
  };

  render() {
    const { number, label } = this.props;

    return (
      <View style={style.card}>
        <Text style={style.label}>{label}</Text>
        <Text style={style.number}>{number}</Text>
      </View>
    );
  }
}
