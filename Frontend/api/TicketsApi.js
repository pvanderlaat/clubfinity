import AsyncStorage from '@react-native-async-storage/async-storage';
import API from './BaseApi';

exports.submitBugReport = async (bugData) => {
  const bearerToken = await AsyncStorage.getItem('userToken');
  try {
    const axiosResponse = await API.post('/api/tickets', {
      type: 'bug',
      bugData,
    },
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    })
      .then(async (response) => (response.data.ok))
      .catch((error) => {
        if (error.response) {
          return { error: error.response.data.error };
        }
        return { error: 'Unable to create new ticket' };
      });
    return axiosResponse;
  } catch (error) {
    console.error(error);
    return false;
  }
};

exports.submitClub = async (
  clubName,
  clubCategory,
  clubDescription,
  tags,
  thumbnailUrl,
  facebookLink,
  instagramLink,
  slackLink,
) => {
  const bearerToken = await AsyncStorage.getItem('userToken');
  const clubData = {
    name: clubName,
    category: clubCategory,
    description: clubDescription,
    tags,
  };
  if (thumbnailUrl) {
    clubData.thumbnailUrl = thumbnailUrl;
  }
  if (facebookLink) {
    clubData.facebookLink = facebookLink;
  }
  if (instagramLink) {
    clubData.instagramLink = instagramLink;
  }
  if (slackLink) {
    clubData.slackLink = slackLink;
  }
  try {
    const resp = await API.post('/api/tickets', {
      type: 'club-submission',
      clubData,
    },
    {
      headers: {
        Authorization: `Bearer ${bearerToken}`,
      },
    })
      .then(async (response) => (response.data.ok))
      .catch((error) => {
        if (error.response) {
          return { error: error.response.data.error };
        }
        return { error: 'Unable to create new ticket' };
      });
    return resp;
  } catch (error) {
    console.error(error);
    return false;
  }
};
